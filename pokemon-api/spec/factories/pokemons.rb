require 'faker'

FactoryBot.define do
  factory :pokemon do
    number     { Faker::Number.between(from: 1, to: 150) }
    name       { Faker::Games::Pokemon.name }
    type1      { 'Grass' }
    type2      { 'Grass' }
    total      { Faker::Number.between(from: 1, to: 700) }
    hp         { Faker::Number.between(from: 1, to: 200) }
    attack     { Faker::Number.between(from: 1, to: 150) }
    defense    { Faker::Number.between(from: 1, to: 150) }
    sp_atk      { Faker::Number.between(from: 1, to: 150) }
    sp_def      { Faker::Number.between(from: 1, to: 150) }
    speed      { Faker::Number.between(from: 1, to: 150) }
    generation { Faker::Number.between(from: 1, to: 7) }
    legendary  { Faker::Boolean.boolean(true_ratio: 0.2) }
  end
end
