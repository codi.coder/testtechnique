class ApplicationController < ActionController::API
    include Response
    include ExceptionHandler
    include ::ActionView::Layouts
    include ActionController::MimeResponds
end
