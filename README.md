# TestTechnique

## Endpoints

### GET all

`/pokemons`

**Note :**
To get a JSON response in Postman, set `Content-Type` and `Accept` to `application/json`, `HTML` by default.

### GET by id

`/pokemons/:id`

**Note :**
To get a JSON response in Postman, set `Content-Type` and `Accept` to `application/json`, `HTML` by default.

### POST

`/pokemons`

**Body :**

```json
{
    "number": 1000,
    "name": "Pokemon",
    "type1": "Grass",
    "type2": "Poison",
    "total": 318,
    "hp": 45,
    "attack": 49,
    "defense": 49,
    "sp_atk": 65,
    "sp_def": 65,
    "speed": 45,
    "generation": 1,
    "legendary": true
}
```

### PUT

`/pokemons/:id`

**Body :**

```json
{
    "name": "Ash"
}
```

### DELETE

`/pokemons/:id`

## Tests

`Faker` is used to create mock data and `Database Cleaner Adapter for ActiveRecord` is used to clean the test database.

Run tests with `bundle exec rspec`.
